//
//  JDMedicalUtils.m
//  JDMedical
//
//  Created by 钟柳 on 05/09/2017.
//  Copyright © 2017 JDMedical. All rights reserved.
//

#import "JDMedicalUtils.h"
#import "AFNetworking.h"
#import <AVFoundation/AVFoundation.h>
#import "SGQRCodeScanningVC.h"
#import "MJRefresh.h"
#import "NSDictionary+JDDictionary.h"
#import "NIMContactSelectViewController.h"
#import "JDAuthorityManage.h"
#import <AssetsLibrary/AssetsLibrary.h>
typedef void(^MyBock)();
typedef void(^TapBlock)();

#define MaxSCale 2.0  //最大缩放比例
#define MinScale 0.5  //最小缩放比例

@interface JDMedicalUtils ()
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)TapBlock tapBlock;

// 记录断网 弱网状态
@property (nonatomic,strong)NSMutableArray *requestArr;
@property (nonatomic,strong)NSMutableDictionary *barHiddenDict;

@end

@implementation JDMedicalUtils
static JDMedicalUtils *instancNetworking = nil;
static AFHTTPSessionManager *instanceManager = nil;

- (NSMutableArray *)requestArr{
    if (!_requestArr) {
        _requestArr = [NSMutableArray array];
    }
    return _requestArr;
}
- (NSMutableDictionary *)barHiddenDict{
    if (!_barHiddenDict) {
        _barHiddenDict = [NSMutableDictionary dictionary];
    }
    return _barHiddenDict;
}
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

+ (id)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instancNetworking = [[super allocWithZone:NULL] init];
    });
    return instancNetworking;
}
// 返回网络对象
+ (AFHTTPSessionManager *)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instanceManager = [AFHTTPSessionManager manager];
    });
    return instanceManager;
}
// 加载中  默认
+ (void)JD_loading{
    [JDMedicalUtils JD_endLoading];
    UIActivityIndicatorView *actview = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREENH_HEIGHT / 2 - 100, 60, 60)];
    view.backgroundColor = RGBA(0, 0, 0, 0.6);
    view.layer.cornerRadius = 10;
    actview.center = CGPointMake(30, 30);
    [view addSubview:actview];
    [actview startAnimating];
    view.tag = 888;
    [[[UIApplication sharedApplication] keyWindow] addSubview:view];
}
+ (void)JD_endLoading{
    [[[[UIApplication sharedApplication] keyWindow] viewWithTag:888] removeFromSuperview];
}
// 网络访问常用方法
- (void)GETURL:(NSString *)url paramaters:(NSDictionary *)params success:(void(^)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject))success{
    [self.manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        JDLog(@"%@",error);
    }];
}
- (void)POSTURL:(NSString *)url paramaters:(NSDictionary *)params success:(void(^)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject))success{
    AFNetworkReachabilityManager*manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    // 存请求
    NSDictionary *dic = @{@"vc" : NSStringFromClass([[JDMedicalUtils getCurrentVC] class]),@"url" : url,@"params" : params,@"func" : success};
    [self.requestArr addObject:dic];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            {
                if ([[[JDMedicalUtils getCurrentVC] class] isEqual:NSClassFromString(@"MineController")]) {
                    return ;
                }
                [[JDMedicalUtils shareInstance] NoNetWorkPage:NO];
            }
                break;
            case AFNetworkReachabilityStatusNotReachable:
            {
                if ([[[JDMedicalUtils getCurrentVC] class] isEqual:NSClassFromString(@"MineController")]) {
                    return ;
                }
                [[JDMedicalUtils shareInstance] NoNetWorkPage:NO];
            }
                break;
                
            default:
                break;
        }
    }];
    [self.manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
    [self.manager POST:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [JDMedicalUtils JD_endLoading];
        [[JDMedicalUtils shareInstance] NoNetWorkPage:YES];
        JDLog(@"%@",responseObject[@"errmsg"]);
        JDLog(@"%@",responseObject);
        success(task,[NSDictionary changeType:responseObject]);
        // 删除请求队列
        for (NSInteger index = 0;index < self.requestArr.count;index ++) {
            NSDictionary *dic = self.requestArr[index];
            if ([url isEqualToString:dic[@"url"]]) {
                [self.requestArr removeObject:dic];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [JDMedicalUtils JD_endLoading];
        JDLog(@"%@",error);
    }];
}
- (void)NoNetWorkPage:(BOOL)connect{
    if (connect) {
        self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] ? [[JDMedicalUtils getCurrentVC].navigationController setNavigationBarHidden:[self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] integerValue]] : nil;
        [[[JDMedicalUtils getCurrentVC].view viewWithTag:1233] removeFromSuperview];
    } else {
        [[[JDMedicalUtils getCurrentVC].view viewWithTag:1233] removeFromSuperview];
        self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] ? [[JDMedicalUtils getCurrentVC].navigationController setNavigationBarHidden:[self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] integerValue]] : nil;
        [self.barHiddenDict setObject:[NSNumber numberWithBool:[JDMedicalUtils getCurrentVC].navigationController.navigationBarHidden] forKey:NSStringFromClass([[JDMedicalUtils getCurrentVC] class])];
        [[JDMedicalUtils getCurrentVC].navigationController setNavigationBarHidden:NO];
        UIView *view = [[UIView alloc] initWithFrame:[JDMedicalUtils getCurrentVC].view.bounds];
        view.tag = 1233;
        view.backgroundColor = JD_F7F7F7;
        UIImageView *imgView = [UIImageView new];
        imgView.center = CGPointMake(SCREEN_WIDTH / 2, 150);
        imgView.bounds = Rect(0, 0, 100, 100);
        imgView.image = [UIImage imageNamed:@"img_nonetwork"];
        UIButton *btn = [[UIButton alloc] initWithFrame:Rect(SCREEN_WIDTH / 2 - 50, 220, 100, 28)];
        [btn setBackgroundColor:JD_Red];
        [btn setTitle:@"重新加载" forState:UIControlStateNormal];
        [btn setTitleColor:JD_White forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 14;
        [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:imgView];
        [view addSubview:btn];
        [[JDMedicalUtils getCurrentVC].view addSubview:view];
    }
}
- (void)btnClick{
    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    if (status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown) {
        return ;
    }
    BOOL requested = NO;
    for (NSInteger index = 0;index < self.requestArr.count;index ++) {
        NSDictionary *dic = self.requestArr[index];
        if ([NSStringFromClass([[JDMedicalUtils getCurrentVC] class]) isEqualToString:dic[@"vc"]]) {
            [JDMedicalUtils JD_loading];
            [[JDMedicalUtils shareInstance] POSTURL:dic[@"url"] paramaters:dic[@"params"] success:dic[@"func"]];
            [self.requestArr removeObject:dic];
            requested = YES;
        }
    }
    if (!requested) {
        [[[JDMedicalUtils getCurrentVC].view viewWithTag:1233] removeFromSuperview];
        self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] ? [[JDMedicalUtils getCurrentVC].navigationController setNavigationBarHidden:[self.barHiddenDict[NSStringFromClass([[JDMedicalUtils getCurrentVC] class])] integerValue]] : nil;
    }
}
// 遍历
+ (void)forInObject:(id)object action:(NSInteger(^)(NSInteger,id))actionBlock complete:(void(^)())completeBlock{
    if (![object isKindOfClass:[NSArray class]] && ![object isKindOfClass:[NSDictionary class]]) {
        completeBlock ? completeBlock() : nil;
        return ;
    }
    NSInteger index = 0;
    BOOL NoComplete = NO;
    if ([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]]) {
        for (id obj in object) {
            NSInteger bol = actionBlock(index++,obj);
            if (bol == ForInStateContinue) {
                continue;
            } else if (bol == ForInStateBreak){
                break;
            } else if (bol == ForInStateNoComplete){
                NoComplete = !NoComplete;
                break;
            }
        }
    }
    !NoComplete ? completeBlock() : nil;
}
// 判断执行
+ (void)isYes:(BOOL)condition Yes:(void(^)())yesBlock No:(void(^)())noBlock{
    condition ? yesBlock() : noBlock();
}
// 根据id 获取控制器
+ (id)getCompentByIdentifier:(NSString *)idStr{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    return [story instantiateViewControllerWithIdentifier:idStr];
}
// 修改状态栏
+ (void)setNav:(UINavigationController *)controller TextColor:(UIStatusBarStyle)style NavBackgroundColor:(UIColor *)color translucent:(BOOL)trans{
    // 修改状态栏文字颜色
    [UIApplication sharedApplication].statusBarStyle = style;
    // 修改状态栏背景颜色
    controller.navigationBar.barTintColor = RGBA(255, 255, 255, 0);
    [color isEqual:JD_Red] ? [controller.navigationBar setBackgroundImage:[UIImage imageNamed:@"img_color"] forBarMetrics:UIBarMetricsDefault] : [controller.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    controller.navigationBar.translucent = trans;
}
// 修改nav
+ (void)changeNavagation:(UIViewController *)controller title:(NSString *)title Sel:(SEL)selector{
    controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back_white"] style:UIBarButtonItemStyleDone target:controller action:selector];
    controller.navigationItem.title = title;
    UIColor * color = JD_White;
    UIFont *font = [UIFont systemFontOfSize:16];
    controller.navigationController.navigationBar.tintColor = JD_White;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:color forKey:NSForegroundColorAttributeName];
    [dict setObject:font forKey:NSFontAttributeName];
    controller.navigationController.navigationBar.titleTextAttributes = dict;
}
+ (void)setNavClear:(UINavigationController *)nav{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    nav.navigationBar.barTintColor = [UIColor clearColor];
    nav.navigationBar.translucent = YES;
    [nav.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage new];
}
// 获取地区
+ (NSArray *)areaDict{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"area.json" ofType:nil];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:NSJSONReadingMutableLeaves error:nil];
    return dict[@"data"][@"areas"];
}
// 正则
+ (BOOL)regExp:(NSString *)str Reg:(NSString *)Reg{
    NSRange range = [str rangeOfString:Reg options:NSRegularExpressionSearch];
    if (range.location != NSNotFound) {
        return YES;
    }
    return NO;
}
// alert 集合
+ (void)alertTarget:(UIViewController *)viewController SetTitle:(NSString *)title setMsg:(NSString *)message setControllerStyle:(UIAlertControllerStyle)controllerStyle Data:(NSArray *)data{
    if (![[[UIApplication sharedApplication] keyWindow] viewWithTag: TOAST_TAG]) {
        __block BOOL needToast = NO;
        [JDMedicalUtils forInObject:data action:^NSInteger(NSInteger index, id obj) {
            needToast = obj[@"func"] ? NO : YES;
            return obj[@"func"] ? ForInStateBreak : ForInStateContinue;
        } complete:^{
            if (!needToast) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:controllerStyle];
                APPDELEGATE.alertController = alertController;
                for (NSDictionary *dict in data) {
                    MyBock block = dict[@"func"];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:dict[@"title"] style:[dict[@"style"] integerValue] handler:^(UIAlertAction * _Nonnull action) {
                        block ? block() : [alertController dismissViewControllerAnimated:YES completion:^{
                            
                        }];
                    }];
                    [alertController addAction:action];
                }
                [viewController presentViewController:alertController animated:YES completion:nil];
            } else {
                JDToast(message);
            }
        }];
    }
}
+ (void)JDToast:(NSString *)tostString complete:(void(^)())complete{
    if ([[[UIApplication sharedApplication] keyWindow] viewWithTag:TOAST_TAG]){
        [[[[UIApplication sharedApplication] keyWindow] viewWithTag:TOAST_TAG] removeFromSuperview];
    }
    [JDMedicalUtils isYes:[JDMedicalUtils getCurrentVC] Yes:^{
        JDLog(@"presentController is %@",[[JDMedicalUtils getCurrentVC] class]);
        [[[JDMedicalUtils getCurrentVC] view] endEditing:YES];
    } No:^{
        
    }];
    CGSize titleSize = [tostString boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 32, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size;
    UILabel *toastLab = [UILabel new];
    toastLab.frame = Rect((SCREEN_WIDTH - titleSize.width - 20) / 2, (SCREENH_HEIGHT - titleSize.height - 20) / 2, titleSize.width + 20, titleSize.height + 20);
    toastLab.text = tostString;
    toastLab.textColor = JD_White;
    toastLab.font = [UIFont systemFontOfSize:14];
    toastLab.backgroundColor = RGBA(0, 0, 0, 0.7);
    toastLab.textAlignment = NSTextAlignmentCenter;
    toastLab.layer.cornerRadius = 8;
    toastLab.layer.masksToBounds = YES;
    toastLab.hidden = YES;
    toastLab.tag = TOAST_TAG;
    [[[UIApplication sharedApplication] keyWindow] addSubview:toastLab];
    [UIView animateWithDuration:0.2 animations:^{
        toastLab.hidden = NO;
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.6 animations:^{
                toastLab.hidden = YES;
            } completion:^(BOOL finished) {
                [toastLab removeFromSuperview];
                complete ? complete() : nil;
            }];
        });
    }];
}
+ (void)JDRemoveToast{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:TOAST_TAG] removeFromSuperview];
}
// mjrefresh
+ (void)MjRefresh:(UITableView *)tableView taerget:(UIViewController *)vc headerRefreshSEL:(SEL)headerFunc footerRefreshSEL:(SEL)footerFunc{
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:vc refreshingAction:headerFunc];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:vc refreshingAction:footerFunc];
}
+ (void)endMjRefresh:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}
+ (void)hideFooter:(UITableView *)tableView{
    [tableView.mj_footer endRefreshingWithNoMoreData];
}
+ (void)showFooter:(UITableView *)tableView{
    [tableView.mj_footer resetNoMoreData];
}
// model 仿 push
+ (void)PushFromController:(UIViewController *)oldController Controller:(UIViewController *)newController complete:(void(^)())complete{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromRight;
    [oldController.view.window.layer addAnimation:animation forKey:nil];
    [oldController.navigationController pushViewController:newController animated:YES];
    complete ? complete() : nil;
}
+ (void)scanQRCode:(UIViewController *)selfController{
    // 1、 获取摄像设备
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (status == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        SGQRCodeScanningVC *vc = [[SGQRCodeScanningVC alloc] init];
                        vc.hidesBottomBarWhenPushed = YES;
                        [JDMedicalUtils PushFromController:selfController Controller:vc complete:^{
                            
                        }];
                    });
                    // 用户第一次同意了访问相机权限
                    JDLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
                    
                } else {
                    // 用户第一次拒绝了访问相机权限
                    JDLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
                }
            }];
        } else if (status == AVAuthorizationStatusAuthorized) { // 用户允许当前应用访问相机
            SGQRCodeScanningVC *vc = [[SGQRCodeScanningVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [JDMedicalUtils PushFromController:selfController Controller:vc complete:^{
                
            }];
        } else if (status == AVAuthorizationStatusDenied) { // 用户拒绝当前应用访问相机
            [[JDAuthorityManage manager] showMsg:@"打开相机"];
        } else if (status == AVAuthorizationStatusRestricted) {
            JDLog(@"因为系统原因, 无法访问相册");
        }
    } else {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"未检测到您的摄像头" preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertC addAction:alertA];
        [selfController presentViewController:alertC animated:YES completion:nil];
    }
}
+ (void)dismissController:(UIViewController *)oldController{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromLeft;
    [oldController.view.window.layer addAnimation:animation forKey:nil];
    //    [oldController dismissViewControllerAnimated:NO completion:^{
    //
    //    }];
    [oldController.navigationController popViewControllerAnimated:NO];
}
// 取控制器
+ (UIViewController *)viewController:(UIView *)view{
    for (UIView* next = [view superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        
        NSLog(@"===%@",[window subviews]);
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    return result;
}
// 本地存储
+ (void)setCache:(id)obj key:(NSString *)key{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"user"]){
        dict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"user"] mutableCopy];
    }
    [dict setObject:obj forKey:key];
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:dict];
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"user"];
}
+ (id)getCacheforKey:(NSString *)key{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return dict[key];
}
// json转换
+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}
//字典转json格式字符串：
+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
}
//json格式字符串转字典：
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        JDLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
+ (id)JSONValue:(NSString *)str;
{
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    __autoreleasing NSError* error = nil;
    if (!data) return nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (error != nil) return nil;
    return result;
}
// md5
+(NSString *)MD5ForLower32Bate:(NSString *)str{
    //要进行UTF8的转码
    const char* input = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02x", result[i]];
    }
    return digest;
}
// 按钮计时
+ (void)mainTarget:(UIButton *)target startTime:(NSUInteger)coutTime{
    if (coutTime > 0) {
        __block NSInteger timeout=coutTime; //倒计时时间
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
        dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
        dispatch_source_set_event_handler(_timer, ^{
            if(timeout < 1){ //倒计时结束，关闭
                dispatch_source_cancel(_timer);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置
                    [target setTitleColor:JD_Red forState:UIControlStateNormal];
                    [target setTitle:@"重新发送" forState:UIControlStateNormal];
                    target.userInteractionEnabled = YES;
                    timeout = 0;
                });
            }else{
                //            int minutes = timeout / 60;
                int seconds = timeout % 60;
                NSString *strTime = [NSString stringWithFormat:@"%.2d", JDOr(seconds, 60)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置
                    target.titleLabel.text = [NSString stringWithFormat:@"%@秒后重发",strTime];
                    [target setTitle:[NSString stringWithFormat:@"%@秒后重发",strTime] forState:UIControlStateNormal];
                    target.userInteractionEnabled = NO;
                    
                });
                timeout--;
                
            }
        });
        dispatch_resume(_timer);
    }
}
// 日期转换
+ (NSDate *)dateStringToDate:(NSString *)string{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSDate *date =[dateFormat dateFromString:[NSString stringWithFormat:@"%@:01",string]];
    return date;
}
+ (void)noneData:(NSString *)noneImageName Controller:(UIViewController *)vc react:(CGRect)react{
    [JDMedicalUtils isYes:noneImageName.length Yes:^{
        if (![vc.view viewWithTag:777]) {
            UIImageView *imgview = [[UIImageView alloc] initWithFrame:react];
            imgview.center = CGPointMake(SCREEN_WIDTH / 2, SCREENH_HEIGHT / 4);
            imgview.image = [UIImage imageNamed:noneImageName];
            imgview.tag = 777;
            [vc.view addSubview:imgview];
            [vc.view bringSubviewToFront:imgview];
        }
    } No:^{
        [[vc.view viewWithTag:777] removeFromSuperview];
    }];
}

// 字符串宽高
+ (CGSize)sizeForString:(NSString *)string Width:(float)width{
    UITextView *textView = [[UITextView alloc] initWithFrame:Rect(0, 10, SCREEN_WIDTH, 40)];
    textView.font = [UIFont systemFontOfSize:14];
    textView.text = string;
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit;
}
+ (CGSize)sizeForString:(NSString *)string Width:(float)width fontSize:(CGFloat)size{
    UITextView *textView = [[UITextView alloc] initWithFrame:Rect(0, 10, SCREEN_WIDTH, 40)];
    textView.font = [UIFont systemFontOfSize:size];
    textView.text = string;
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit;
}
+ (float)heightForString:(NSString *)string Width:(float)width{
    CGSize size = [JDMedicalUtils sizeForString:string Width:width];
    return size.height;
}
+ (float)widthForString:(NSString *)string Width:(float)width{
    CGSize size = [JDMedicalUtils sizeForString:string Width:width];
    return size.width;
}
// 选择器
+ (void)presentMemberSelectorAddTeam:(BOOL)addTeam userArr:(NSMutableArray *)array complete:(ContactSelectFinishBlock)block{
    NSMutableArray *users = [[NSMutableArray alloc] init];
    //使用内置的好友选择器
    NIMContactFriendSelectConfig *config = [[NIMContactFriendSelectConfig alloc] init];
    //获取自己id
    NSString *currentUserId = [[NIMSDK sharedSDK].loginManager currentAccount];
    [users addObject:currentUserId];
    //将自己的id过滤
    config.addTeam = addTeam;
    config.filterIds = users;
    //需要多选
    config.needMutiSelected = YES;
    config.userArray = array ? array : nil;
    //初始化联系人选择器
    NIMContactSelectViewController *vc = [[NIMContactSelectViewController alloc] initWithConfig:config];
    //回调处理
    vc.finshBlock = block;
    [vc show];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}
// 分享
+ (void)shareParams:(NSDictionary *)params result:(void(^)(NSError *))res{
    // 构造出具体会话
    JDLog(@"id ++ ;%@",params[@"userid"]);
    NIMSession *session  = [NIMSession session:params[@"userid"] type:([params[@"userid"] length] > 9 || [params[@"userid"] length] == 9) ? NIMSessionTypeTeam :  NIMSessionTypeP2P];
    JDShareManage *attachment = [[JDShareManage alloc] init];
    attachment.title = params[@"title"];
    attachment.desc = params[@"desc"];
    attachment.imgUrl = params[@"imgurl"];
    attachment.orgid = params[@"orgid"];
    attachment.orgtype = params[@"orgtype"];
    
    NIMCustomObject *object    = [[NIMCustomObject alloc] init];
    object.attachment = attachment;
    
    // 构造出具体消息并注入附件
    NIMMessage *message               = [[NIMMessage alloc] init];
    NIMCustomObject *customObject     = [[NIMCustomObject alloc] init];
    customObject.attachment           = attachment;
    message.messageObject             = customObject;
    message.apnsContent = @"好友分享";
    // 错误反馈对象
    NSError *error = nil;
    // 发送消息
    [[NIMSDK sharedSDK].chatManager sendMessage:message toSession:session error:&error];
    res(error);
}
+ (void)statusChangeWithBtn:(UIButton *)btn currentTextFiled:(UITextField *)current ReplaceString:(NSString *)string textFiledArr:(NSArray<UITextField *> *)textFiledArr{
    UITextField *initTf = textFiledArr[0];
    BOOL result = initTf.text.length || [current isEqual:initTf];
    for (UITextField *tf in textFiledArr) {
        result = result && (tf.text.length || [current isEqual:tf]);
    }
    if ((string.length ? : [[NSString stringWithFormat:@"%@%@",current.text,string] length] - 1) && result) {
        btn.backgroundColor = JD_Red;
        btn.userInteractionEnabled = YES;
    } else {
        btn.backgroundColor = JD_DDD;
        btn.userInteractionEnabled = NO;
    }
}

@end

