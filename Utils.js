// 外部暴露对象
let Layer = null,
	exportObj = null,
	JQueryVue = null;

const RegExpMenu = {
	NUM_6: /^\d{6}$/,
};
// 初始化layer层
layui.use('layer', () => {

});

const Methods = {
	getStrByteLength(str) {
		return str.replace(/[\u0391-\uFFE5]/g, "aa").length;
	},
	//1.路由name   2.路由传参
	go(path, query) {
		layer.closeAll();
		query = query || {};
		this.$router.push({
			path,
			query
		});
	},
	downloadWithUrl(url) {
		let $eleForm = null;
		if($('form#AppdownloadForm').length) {
			$eleForm = $('form#AppdownloadForm');
		} else {
			$eleForm = $("<form id='AppdownloadForm' method='get'></form>");
			$(document.body).append($eleForm);
		}
		$eleForm.attr("action", url);
		//提交表单，实现下载
		$eleForm.submit();
	},
	toast(msg, callback, time) {
		if(Layer) return;
		Layer = layer.msg(msg, {
			time: time || 2500,
			index: 128,
		}, () => {
			Layer = null;
			callback && typeof(callback) === 'function' && callback();
		})

	},
	getFileImg(input, callBack) {
		let reader = new FileReader();
		reader.readAsDataURL($(input)[0].files[0]);
		reader.onload = (e) => {
			callBack && callBack(e.target.result);
		}
	},
	viewContractFile(contractCode, companyCode) {
		window.open('http://localhost:9800/#/ContractViewFile?contractCode=' + contractCode + '&companyCode=' + companyCode);
	},
	getFaceVerifyToken(option) {
		this.ajaxRequest({
			url: 'user/cacheFaceCode',
			success: (code) => {
				let returnUrl = 'http://ldTest.ooteco.com/#/FaceDiscriminate?tokenMd5=' + this.getValueForKey('loginRespones').tokenMd5 + '&randomCode=' + code.data.faceCode;
				if(option.type == 2) {
					returnUrl += '&companyCode=' + option.companyCode + '&contractCode=' + option.contractCode;
				}
				this.ajaxRequest({
					url: 'tripartite/getBizToken',
					params: {
						id: option.cardNo,
						name: option.name,
						returnUrl
					},
					success: (res) => {
						layer.confirm('', {
							title: false,
							content: `<div class="row flexHcenter">
								<div id="qrcode" class="flexHcenter" style="width: 15rem;height: 15rem;">
								</div>
							</div>
							<div class="row marginTop16 c9696A0 f12 talincenter" >` + (option.type == 2 ? '微信扫描上方二维码进入合同签署' : '微信扫描上方二维码进入人脸识别') + `</div>`,
							btnAlign: 'c',
							resize: false,
							scrollbar: false,
							btn: ['关闭'],
							btn1: () => {
								layer.closeAll();
							}
						})
						if(!JSON.parse(res.data).biz_token) {
							this.toast("合同签署二维码生成失败");
							return false;
						}
						let urlPath = 'https://openapi.faceid.com/lite/v1/do/' + JSON.parse(res.data).biz_token;
						console.log(urlPath);
						var qrcode = new QRCode("qrcode", {
							text: urlPath,
							width: 150,
							height: 150,
							colorDark: "#000000",
							colorLight: "#ffffff",
							correctLevel: QRCode.CorrectLevel.H
						});
					}
				})
			}
		})
	},
	askForUser(msg, success, fail) {
		if(typeof(msg) === 'string') {
			msg = {
				msg,
				btn: ['取消', '确认'],
			}
		} else {
			msg.btn = msg.btn || ['取消', '确认'];
		}
		layer.confirm('', {
			title: false,
			content: msg.msg,
			btnAlign: 'c',
			resize: false,
			scrollbar: false,
			btn: msg.btn,
			btn1: () => {
				fail && typeof(fail) === 'function' && fail();
				layer.closeAll();
			},
			btn2: () => {
				success && typeof(success) === 'function' && success();
				return false;
			}
		})
	},
	//路由回退
	back(dom, num = -1) {
		typeof dom === 'number' && (num = dom);
		layer.closeAll();
		this.$router.go(num);
	},
	randomTxt(len) {
		len = len || parseInt(Math.random() * 6) + 4
		let word = '';
		for(let i = 0; i < len; i++) {
			word += String.fromCodePoint(Math.round(Math.random() * 20901) + 19968);
		}
		//		return word;
		return '--';
	},
	// 配置文件初始化
	configInit(callback) {
		this.ajaxRequest({
			url: 'user/getAllPermissionList',
			success: res => {
				callback && typeof(callback) == 'function' && callback();
				this.setValueForKey('permissionList', res.data);
			}
		});
	},
	deepClone(obj) {
		let isClass = (o) => {
			if(o === null) return "Null";
			if(o === undefined) return "Undefined";
			return Object.prototype.toString.call(o).slice(8, -1);
		}
		var result, oClass = isClass(obj);
		//确定result的类型
		if(oClass === "Object") {
			result = {};
		} else if(oClass === "Array") {
			result = [];
		} else {
			return obj;
		}
		for(key in obj) {
			var copy = obj[key];
			if(isClass(copy) == "Object") {
				result[key] = arguments.callee(copy); //递归调用
			} else if(isClass(copy) == "Array") {
				result[key] = arguments.callee(copy);
			} else {
				result[key] = obj[key];
			}
		}
		return result;
	},
	getPermissionList(callBack) {
		let func = (datas, levelArr) => {
			datas.forEach((item, index) => {
				levelArr = this.deepClone(levelArr);
				levelArr.push(item)
				if(item.subPermissions) {
					index && levelArr.splice(levelArr.length - 2, 1);
					func(item.subPermissions, levelArr);
				} else {
					if(index == datas.length - 1) {
						callBack && callBack(levelArr);
					}
				}
			})
		}
		func(this.getValueForKey('permissionList'), []);
	},
	setValueForKey(key, val) {
		let localInfo = !!localStorage.getItem('localInfo') ? JSON.parse(localStorage.getItem('localInfo')) : (new Object());
		localInfo[key] = val;
		console.log(localInfo);
		localStorage.setItem('localInfo', JSON.stringify(localInfo));
	},
	getValueForKey(key) {
		if(!localStorage.getItem('localInfo')) {
			return {};
		}
		let localInfo = JSON.parse(localStorage.getItem('localInfo'));
		return localInfo[key] || {};
	},

	signOut() {
		if(confirm('确定退出登录?')) {
			localStorage.clear();
			this.go('/Login/Login');
		}
	},
	RegObject(Arr) {
		// ['EN','NUM','FIX']
		if(typeof(Arr) === 'string') {
			// 非数组
			return RegExpMenu[Arr];
		}
		let type = Arr.length == 2 ? 'FIX' : Arr[2];
		let RegString = Arr.length > 3 ? Arr[3] : '';
		let regStr = {
			NUM: "0-9",
			EN: "a-zA-Z",
			FIX: "a-zA-Z0-9",
			CN: `\u4e00-\u9fa5`,
			LEN: "{" + Arr[0] + "," + Arr[1] + "}"
		}
		type.split(',').forEach((item) => {
			RegString += regStr[item];
		})
		return new RegExp('[' + RegString + ']' + regStr.LEN + '$');
	},
	// 数据检验
	dataVerify(obj) {
		if(!(typeof(obj) === 'object')) {
			return obj;
		} else if(!obj.val) {
			if(obj.val === 0) {
				return obj.val;
			}
			this.toast(obj.msg || (obj.name + '不能为空！'));
			return false;
		} else {
			if(obj.msg && typeof(obj.msg) == 'function') {
				obj.msg = obj.msg(obj);
			}
			if(!obj.reg || (!obj.name && !obj.msg)) {
				return obj.val;
			} else if(obj.val.match(this.RegObject(obj.reg))) {
				return obj.val;
			} else {
				this.toast(obj.msg || obj.name + '输入有误！');
				return false;
			}
		}
	},
	getBlobBydataURI(dataURI, type) {
		var binary = atob(dataURI.split(',')[1]);
		var array = [];
		for(var i = 0; i < binary.length; i++) {
			array.push(binary.charCodeAt(i));
		}
		return new Blob([new Uint8Array(array)], {
			type: type
		});
	},
	postFormData(option) {
		!option.NoLoading && this.loading(true);
		$.ajax({
			url: this.$store.state.BaseStore.Api + option.url,
			type: "post",
			headers: {
				token: this.getValueForKey('loginRespones').token
			},
			data: option.formData,
			processData: false,
			contentType: false,
			success: data => {
				option.success && typeof(option.success) === 'function' && option.success(data);
			},
			error: err => {
				option.err && typeof(option.err) === 'function' && option.err(data);
			},
			complete: () => {
				option.complete && option.complete();
				!option.NoLoading && this.loading(false);
			}
		});
	},
	convertBase64UrlToBlob(urlData) {
		var bytes = window.atob(urlData.split(',')[1]); //去掉url的头，并转换为byte
		//处理异常,将ascii码小于0的转换为大于0
		var ab = new ArrayBuffer(bytes.length);
		var ia = new Uint8Array(ab);
		for(var i = 0; i < bytes.length; i++) {
			ia[i] = bytes.charCodeAt(i);
		}
		return new Blob([ab], {
			type: 'image/png'
		});
	},
	//数据请求方法封装
	ajaxRequest(paramJson) {

		if(!paramJson) {
			console.warn('参数不能为空');
		}
		//						paramJson.url = 'api_hx/' + paramJson.url;
		paramJson.url = this.$store.state.BaseStore.Api + paramJson.url;

		let continueProcess = true;
		$.each(paramJson.params, (key, val) => {
			if(val && typeof(val) === 'object') {
				let res = this.dataVerify(val);
				if(!!res) {
					paramJson.params[key] = res || '';
				} else {
					continueProcess = false;
					return false;
				}
			}
		});
		if(!continueProcess) return false;
		let headers = paramJson.headers || {};
		headers["token"] = this.getValueForKey('loginRespones') && this.getValueForKey('loginRespones')['token'];
		$.ajax({
			type: paramJson.type || 'get',
			url: paramJson.url,
			headers,
			data: paramJson.params || {},
			dataType: 'json',
			async: true,
			success: (response) => {
				if(!paramJson.interrecept && parseInt(response.code) != 0) {
					this.toast(response.msg);
					return false;
				}
				paramJson.success(response)
				console.log("success");
			},
			error: (err) => {
				paramJson.error && paramJson.error(err);
			},
			complete: () => {
				console.log("complete");
				paramJson.complete && paramJson.complete();
			}
		});
	},

	//隐藏显示loading
	loading(str) {

		if(typeof str != 'undefined' && typeof str == 'boolean') {
			this.$store.state.BaseStore.Loading = str;
			return;
		}
		this.$store.state.BaseStore.Loading = !this.$store.state.BaseStore.Loading
	},

	//获取当前时间
	currentDate() {
		let date = new Date();
		return date.getFullYear() + '-' + ((date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + (date.getDate() > 10 ? date.getDate() : ('0' + date.getDate())) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}
}