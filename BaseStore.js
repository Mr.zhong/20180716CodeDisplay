const state = {
	// API环境配置项
//	Api : '/api_jp/', 
//	Api : '/api_cs/',
	Api : '/api_manager/',
//	Api : '/api_hx/',
//	Api : '/api_zk/',
    Loading : false, // 全局加载
    fileRootUrl : '/api_manager/chainFileStore/',  // 文件根地址
    faceVerify : 'https://openapi.faceid.com/lite/v1/do/', // 人脸识别根地址
    wsObj : null,  // websoket 对象
    webSocketState : 0,  // websoket 对象状态
}

const getters = {

}

const actions = {
	// 观察者模式
	setSendEvent(store, params) {
		let event_name = params.event_name;
		let recordArr = [];
		// 遍历
		let process = (objArr) => {
			let result = null;
			$.each(objArr, function(index, objc) {
				let subProcess = (obj) => {
					let levelArr = obj.$route.fullPath.split('/'),
						condition = null;
					if (levelArr.length == 4){
						condition = $(obj.$el).is($('div#sub_sub_router>div'));
					} else {
						condition = obj.$route.fullPath.indexOf('Login/') > -1 ? $(obj.$el).is($('div#main_router>div')) : $(obj.$el).is($('div#sub_router>div'));
					}
					if((event_name in obj) && condition) {
						return obj;
					} else {
						if(obj.$children && obj.$children.length) {
							return process(obj.$children);
						}
					}
				};
				result = subProcess(objc);
				if (!!result){
					return false;
				}
			});
			return result;
		}
		let vm = process([exportObj]);
		if(!!vm && (event_name in vm)) {
			vm[event_name](params.extra);
		} else {
			console.warn("invalid event '" + event_name + "'");
		}
		return false;
		store.commit('setSendEvent', params);
	}
}

const mutations = {

}

export default {
	state,
	getters,
	actions,
	mutations
}